﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CM.Web.DataTableModel
{
    public class OfficialDataTableModel
    {
        public int Id { get; set; }
        public string Name { get
            {
                return string.Join(" ", FirstName, LastName, Suffix);
            }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string Committee { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
    }
}