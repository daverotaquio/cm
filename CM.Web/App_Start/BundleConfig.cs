﻿using System.Web;
using System.Web.Optimization;

namespace CM.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/bower_components/jquery/dist/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/bower_components/bootstrap/dist/js/bootstrap.min.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/lightGalleryJs").Include(
                       "~/bower_components/lightgallery/dist/js/lightgallery-all.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/Scripts/app.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/theme.css"));

            bundles.Add(new StyleBundle("~/bundles/fontawesome").Include(
                      "~/bower_components/font-awesome/css/font-awesome.css"));
            
            bundles.Add(new StyleBundle("~/bundles/lightGalleryCss").Include(
                      "~/bower_components/lightgallery/dist/css/lightgallery.min.css",
                      "~/bower_components/lightgallery/dist/css/lg-transitions.min.css"));

            bundles.Add(new StyleBundle("~/bundles/landing").Include(
                      "~/Content/landing.css"));

            bundles.Add(new StyleBundle("~/bundles/style").Include(
                      "~/Content/style.css"));
        }
    }
}
