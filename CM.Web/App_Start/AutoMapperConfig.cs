﻿using AutoMapper;
using CM.Core.Entities;
using CM.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CM.Web.App_Start
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration MapperConfiguration;

        public static void RegisterMappings()
        {
            MapperConfiguration = new MapperConfiguration(cfg =>
            {
                // Mapping goes here
                cfg.CreateMap<Official, OfficialViewModel>().ReverseMap();
            });
        }
    }
}