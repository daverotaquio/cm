﻿using AutoMapper;
using CM.Web.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CM.Web.Controllers
{
    public class BaseController : Controller
    {
        private IMapper _mapper;
        protected IMapper MapperInstance => _mapper ?? (_mapper = AutoMapperConfig.MapperConfiguration.CreateMapper());
    }
}