﻿$(document).ready(function () {
    $('.dropdown-submenu a').on("click", function (e) {
        $(this).css("background-color", "#f5f5f5");

        $(this).next('ul').slideToggle(function () {
            var display = $(".level-two").css("display");

            if (display === "none") {
                $(document).find(".dropdown-submenu-a").css("background-color", "#fff");
            }
        });

        e.stopPropagation();
        e.preventDefault();
    });

    $(document).on("click", ".search-button", function (e) {
        e.preventDefault();

        var searchButtonHasClass = $(this).parent("li").hasClass("active");

        var searchFormContainerHasClass = $(".search-form-container").hasClass("search-input-visible");

        var navItemHasClass = $(".nav-item").hasClass("nav-item-hidden");

        if (searchFormContainerHasClass) {
            // perform ajax search
        }
        else {
            if (!searchButtonHasClass) {
                $(this).parent("li").addClass("active");
            }
            else {
                $(this).parent("li").removeClass("active");
            }

            if (!searchFormContainerHasClass) {
                $(".search-form-container").addClass("search-input-visible");
            }
            else {
                $(".search-form-container").removeClass("search-input-visible");
            }

            if (!navItemHasClass) {
                $(".nav-item").addClass("nav-item-hidden");
            }
            else {
                $(".nav-item").removeClass("nav-item-hidden");
            }
        }


    });

    $(document).on("click", function () {
        if (!$(document.activeElement).hasClass("search-button") && !$(document.activeElement).hasClass("search-input")) {
            $(".search-button").parent("li").removeClass("active");
            $(".search-form-container").removeClass("search-input-visible");
            $(".nav-item").removeClass("nav-item-hidden");
        }
    });
});