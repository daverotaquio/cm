﻿var utilities = new function () {
    return {
        init: function () {
            this.self = utilities;
        },
        form: {
            ajax: function (obj) {
                if (typeof obj === "undefined") {
                    return null;
                }
                var url = obj.url();
                var type = obj.type;
                var dataType = obj.dataType();
                var data = obj.data();
                var processData = obj.processData;
                var contentType = obj.contentType;

                return $.ajax({
                    url: url,
                    type: type,
                    dataType: dataType,
                    data: data,
                    processData: processData,
                    contentType: contentType,
                    beforeSend: function (data) {
                        if (obj.hasOwnProperty("beforeSend")) {
                            if (jQuery.isFunction(obj.beforeSend)) {
                                obj.beforeSend(data);
                            }
                        }
                    },
                    success: function (data) {
                        if (obj.hasOwnProperty("success")) {
                            if (jQuery.isFunction(obj.success)) {
                                obj.success(data);
                            }
                        }
                    },
                    error: function (e) {
                        if (obj.hasOwnProperty("error")) {
                            if (jQuery.isFunction(obj.error)) {
                                obj.error(e);
                            }
                        }
                    },
                    complete: function (xhr, textStatus) {
                        if (obj.hasOwnProperty("complete")) {
                            if (jQuery.isFunction(obj.complete)) {
                                obj.complete(data, textStatus, xhr);
                            }
                        }
                    },
                });
            },
            // Add forms that is dynamically injected in DOM
            // Call this method in success call back of the ajax request
            JQValidateParse: function (form) {
                $(form).removeData("validator");
                $(form).removeData("unobtrusiveValidation");
                $.validator.unobtrusive.parse($(form));
            }
        }
    }
}
