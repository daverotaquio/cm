﻿"use strict";

$(document).ready(function () {
    adminOfficersIndex.initDataTable();

    $(document).on("click", ".add-official", function () {
        adminOfficersIndex.createEdit("#official-createedit-form", 0, "Add new official");
    });

    $(document).on("click", ".edit-official", function () {
        var entityId = $(this).data("entity-id");

        adminOfficersIndex.createEdit("#official-createedit-form", entityId, "Edit official information");
    });

    $(document).on("click", ".delete-official", function () {
        var entityId = $(this).data("entity-id");

        adminOfficersIndex.delete(entityId, "Delete official");
    });
});

var adminOfficersIndex = {
    officialsTable: null,
    initDataTable: function () {
        adminOfficersIndex.officialsTable = $(".officials-table").DataTable({
            ajax: {
                url: "/admin/spofficials/list",
                dataSrc: ""
            },
            columns: [
                {
                    data: "Id",
                    render: function (data) {
                        return '<a class="edit-official btn-link empty-href" data-entity-id="' + data + '">Edit</a> | <a class="btn-link delete-official empty-href" data-entity-id="' + data + '">Delete</a>';
                    }
                },
                { data: "Name" },
                { data: "Committee" },
                { data: "MobileNumber" },
                { data: "Email" }

            ]
        });
    },
    createEdit: function (form, id, title) {
        BootstrapDialog.show({
            message: $("<div></div>").load("/admin/spofficials/createedit/" + id),
            type: BootstrapDialog.TYPE_DEFAULT,
            title: title,
            buttons: [{
                label: "Save",
                cssClass: "btn-primary",
                action: function (dialog) {
                    utilities.form.ajax({
                        url: function () {
                            return "/admin/spofficials/createedit";
                        },
                        dataType: function () {
                            return "json";
                        },
                        type: "POST",
                        data: function () {
                            return $(form).serialize();
                        },
                        success: function (data) {
                            if (data.success) {
                                adminOfficersIndex.officialsTable.ajax.reload(null, false);
                                dialog.close();
                            } else {
                                return false;
                            }
                        },
                        error: function (e) {
                            return false;
                            console.log("error");
                            console.log(e);
                        }
                    });
                }
            }, {
                label: "Cancel",
                action: function (dialog) {
                    dialog.close();
                }
            }]
        });
    },
    delete: function (id, title) {
        BootstrapDialog.show({
            message: "Are you sure you want to delete this official?",
            type: BootstrapDialog.TYPE_DEFAULT,
            title: title,
            buttons: [{
                label: "Delete",
                cssClass: "btn-danger",
                action: function (dialog) {
                    utilities.form.ajax({
                        url: function () {
                            return "/admin/spofficials/delete";
                        },
                        dataType: function () {
                            return "json";
                        },
                        type: "POST",
                        data: function () {
                            return { id: id };
                        },
                        success: function (data) {
                            if (data.success) {
                                adminOfficersIndex.officialsTable.ajax.reload(null, false);
                                dialog.close();
                            } else {
                                return false;
                            }
                        },
                        error: function (e) {
                            return false;
                            console.log("error");
                            console.log(e);
                        }
                    });
                }
            }, {
                label: "Cancel",
                action: function (dialog) {
                    dialog.close();
                }
            }]
        });
    }
};