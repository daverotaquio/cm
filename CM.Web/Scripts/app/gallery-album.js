﻿$(document).ready(function () {
    $(".lightGallery").lightGallery({
        selector: ".thumbnail",
        thumbnail: false,
        hash: false,
        pager: false,
        exThumbImage: "data-exthumbimage"
    });
});