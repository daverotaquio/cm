﻿using CM.Web.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CM.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            AutoMapperConfig.RegisterMappings();
        }

        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    //You don't want to redirect on posts, or images/css/js
        //    var isGet = HttpContext.Current.Request.RequestType.ToLowerInvariant().Contains("get");
        //    if (!isGet || HttpContext.Current.Request.Url.AbsolutePath.Contains(".") != false) return;

        //    var lowercaseURL = (Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.Url.AbsolutePath);

        //    if (!Regex.IsMatch(lowercaseURL, @"[A-Z]")) return;
        //    //You don't want to change casing on query strings
        //    lowercaseURL = lowercaseURL.ToLower() + HttpContext.Current.Request.Url.Query;

        //    Response.Clear();
        //    Response.Status = "301 Moved Permanently";
        //    Response.AddHeader("Location", lowercaseURL);
        //    Response.AddHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        //    Response.AddHeader("Pragma", "no-cache");
        //    Response.AddHeader("Expires", "0");
        //    Response.End();
        //}
    }
}
