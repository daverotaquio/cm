﻿using CM.Core.Entities;
using CM.Core.Interfaces.Base;
using CM.Core.Interfaces.Services;
using CM.Web.Controllers;
using CM.Web.DataTableModel;
using CM.Web.Models;
using Exceptionless;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CM.Web.Areas.Admin.Controllers
{
    public class SPOfficialsController : BaseController
    {
        private readonly IOfficialService _officialService;

        public SPOfficialsController(IOfficialService officerService)
        {
            _officialService = officerService;
        }
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            var officials = _officialService.Get();

            var officialList = officials.OrderByDescending(s => s.Id).Select(official => new OfficialDataTableModel
            {
                Id = official.Id,
                FirstName = official.FirstName,
                LastName = official.LastName,
                Suffix = official.Suffix,
                Committee = official.Committee,
                MobileNumber = official.MobileNumber,
                Email = official.Email
            });

            return Json(officialList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreateEdit(int id = 0)
        {
            Official official = new Official();

            if (id > 0)
            {
                official = _officialService.GetById(id);
            }

            return PartialView("_CreateEdit", official);
        }

        [HttpPost]
        public ActionResult CreateEdit(Official model)
        {
            ServiceOperationResult<Official> result = new ServiceOperationResult<Official>();

            try
            {
                result = model.Id > 0 ? _officialService.Update(model) : _officialService.Insert(model);
            }
            catch (Exception e)
            {
                e.ToExceptionless().Submit();
                return Json(new { success = result.Success, error = e.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = result.Success }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            ServiceOperationResult<Official> result = new ServiceOperationResult<Official>();

            try
            {
                result = _officialService.Delete(id);
            }
            catch (Exception e)
            {
                e.ToExceptionless().Submit();
                return Json(new { success = result.Success, error = e.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = result.Success }, JsonRequestBehavior.AllowGet);
        }
    }
}