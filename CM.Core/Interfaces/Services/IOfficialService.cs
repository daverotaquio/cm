﻿using CM.Core.Entities;
using CM.Core.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CM.Core.Interfaces.Services
{
    public interface IOfficialService : IService<Official>
    {
    }
}
