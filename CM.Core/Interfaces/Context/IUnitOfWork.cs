﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CM.Core.Interfaces.Context
{
    public interface IUnitOfWork : IDisposable
    {
        //generic way of instantiating repository
        IRepository<T> Repository<T>() where T : class;
        int Complete();
    }
}
