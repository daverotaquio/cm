﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CM.Core.Interfaces.Base
{
    public class ServiceOperationResult<T> where T : class
    {
        /// <summary>
        /// Object to be return to the caller
        /// </summary>
        public T Data { get; set; }

        public bool Success { get; set; }

        public int ErrorCode { get; set; }
    }
}
