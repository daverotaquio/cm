﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CM.Core.Interfaces.Base
{
    public interface IService<T> where T : class
    {
        ServiceOperationResult<T> Insert(T model);
        ServiceOperationResult<T> Update(T model);
        ServiceOperationResult<T> Delete(int id);
        T GetById(int id);
        IEnumerable<T> Get();

        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, bool enableTracking = false,
            params Expression<Func<T, object>>[] includeProperties);
    }
}
