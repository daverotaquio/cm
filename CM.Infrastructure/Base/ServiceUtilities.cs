﻿using CM.Core.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CM.Infrastructure.Base
{
    public class ServiceUtilities<T> where T : class
    {
        public ServiceOperationResult<T> SaveGenericWithReturn(Func<object> action)
        {
            ServiceOperationResult<T> result = new ServiceOperationResult<T>();
            try
            {
                var actionResult = action();
                var resultData = actionResult as T;
                result.Data = resultData;
                result.Success = true;

            }
            catch (Exception)
            {
                result.Success = false;
                throw;
            }

            return result;

        }

        public ServiceOperationResult<T> Delete(Func<object> action)
        {
            ServiceOperationResult<T> result = new ServiceOperationResult<T>();

            try
            {
                action();
                result.Success = true;

            }
            catch (Exception)
            {
                result.Success = false;
                throw;
            }

            return result;

        }
    }
}
