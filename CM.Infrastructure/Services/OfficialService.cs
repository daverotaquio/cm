﻿using CM.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CM.Core.Entities;
using CM.Core.Interfaces.Base;
using System.Linq.Expressions;
using CM.Core.Interfaces.Context;
using CM.Infrastructure.Base;
using Exceptionless;

namespace CM.Infrastructure.Services
{
    public class OfficialService : IOfficialService
    {
        private IUnitOfWork _unitOfWork;
        private ServiceUtilities<Official> serviceUtility = new ServiceUtilities<Official>();
        public OfficialService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ServiceOperationResult<Official> Delete(int id)
        {
            ServiceOperationResult<Official> result = new ServiceOperationResult<Official>();
            try
            {
                _unitOfWork.Repository<Official>().Remove(id);
                _unitOfWork.Complete();

                result.Success = true;
                return result;
            }
            catch (Exception e)
            {
                e.ToExceptionless().Submit();
                throw;
            }
        }

        public IEnumerable<Official> Get()
        {
            return _unitOfWork.Repository<Official>().Get();
        }

        public Official GetById(int id)
        {
            return _unitOfWork.Repository<Official>().GetById(id);
        }

        public ServiceOperationResult<Official> Insert(Official model)
        {
            return serviceUtility.SaveGenericWithReturn(() =>
            {
                _unitOfWork.Repository<Official>().Add(model);
                _unitOfWork.Complete();
                return model;
            });
        }

        public ServiceOperationResult<Official> Update(Official model)
        {
            return serviceUtility.SaveGenericWithReturn(() =>
            {
                _unitOfWork.Repository<Official>().Update(model);
                _unitOfWork.Complete();
                return model;
            });
        }

        public IEnumerable<Official> Get(Expression<Func<Official, bool>> filter = null, Func<IQueryable<Official>, IOrderedQueryable<Official>> orderBy = null, bool enableTracking = false, params Expression<Func<Official, object>>[] includeProperties)
        {
            return _unitOfWork.Repository<Official>().Get(filter, orderBy, enableTracking, includeProperties);
        }
    }
}
