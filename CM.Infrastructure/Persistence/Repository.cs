﻿using CM.Core.Interfaces.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CM.Infrastructure.Persistence
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        internal CMContext context;
        internal DbSet<TEntity> dbset;

        public Repository(CMContext context)
        {
            this.context = context;
            this.dbset = context.Set<TEntity>();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, bool enableTracking = false, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = dbset;


            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
                query = orderBy(query);

            return enableTracking ? query.ToList() : query.AsNoTracking().ToList();
        }

        public TEntity GetById(object id)
        {
            return dbset.Find(id);
        }

        public TEntity SignleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return dbset.Where(predicate).FirstOrDefault();
        }

        public void Add(TEntity entity)
        {
            dbset.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            dbset.AddRange(entities);
        }

        public void Remove(TEntity entity)
        {
            dbset.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            dbset.RemoveRange(entities);
        }

        public void Update(TEntity entityToUpdate)
        {
            dbset.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public void Remove(object id)
        {
            var entity = dbset.Find(id);
            dbset.Remove(entity);
        }
    }
}
