namespace CM.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update02 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Officers", newName: "Officials");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Officials", newName: "Officers");
        }
    }
}
